const getSum = (str1, str2) => {
  if(typeof str1 != 'string' && typeof str2 != 'string' || isNaN(str1) && isNaN(str2)){
    return false;
  }

  if(str1 == ''){
    str1 = '0';
  }
  else if(str2 == ''){
      str2 = '0';
  }
  
  return (parseInt(str1) + parseInt(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countP = 0;
  let countC = 0;
  Object.values(listOfPosts).forEach(item =>{
    Object.values(item).forEach(x =>{
      if(x == authorName){
        countP++;
      }
    });
  });
  Object.values(listOfPosts).forEach(item =>{
      Object.values(item).forEach(x => {
          Object.values(x).forEach(y => {
              if(y.author == authorName){
                  countC++;
              }
          })
      })
  })
    return `Post:${countP},comments:${countC}`;
};

const tickets=(people)=> {
  let change = 0;
  let cost = 25;
  let Hchange = 0;
  for(let i = 0; i < people.length; i++){
    if(cost - people[i] < 0){
      if(change < 0){
        Hchange -= 1;
      } else if (change - (people[i] - cost) >= 0){
				  change += people[i] - cost;
					Hchange += 1;
					} else {
						Hchange -= 1; 
					}
    } else {
      change += cost;
      Hchange += 1;
    }
  }
  if(people.length == Hchange){
    return 'YES';
  } else {
    return 'NO';
  }
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
